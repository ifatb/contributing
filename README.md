# Contribution guide for Sample Data Templates

The `sample-data-templates` group is used to store the original sources of Sample Data Template projects.

These projects are then exported as tarballs and added to the main application for use with the Sample
Data Templates feature.

## Contributing a new sample data project template

If you'd like to contribute a new built-in sample data project template to be distributed with GitLab, please do the following:

### 1. Create the Project as desired in this group

Create a new public project in this group with the project content you'd like to contribute in a namespace of your choosing with all the data as required.

You can view a working example [here](https://gitlab.com/gitlab-org/sample-data-templates/sample-gitlab-project).

### 2. Set the Due Dates in the project (optional)

The Sample Data feature has a unique function where it will move any Due Dates set in Issues and Milestones forward at the time of import to make them relevant.

This works by finding the due date closest to the median of all due dates and moving it forward to the import date. Along with this all other due dates are moved forward by the same amount as well meaning the spread of due dates as set in the original project are maintained but kept relevant (i.e. half will be set in the past and the other half in the future roughly).

To have due dates set then in the project that will always be relevant simply add them to the original Project's Issues and Milestones as required. The feature will take care of the rest.

### 3. Add the project to the GitLab application

To make the sample data template available when creating a new project in the GitLab application the vendoring process will have to be completed as detailed below.

Note if you wish to have the project shown in the templates list with a custom icon you should refer to the [3a. Add a template list icon for the Sample Data project](#3a-add-a-template-list-icon-for-the-sample-data-project) below.

1. Create a new branch of the main GitLab repo and add details of the template in the following code locations:
    * [`lib/gitlab/sample_data_template.rb`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/sample_data_template.rb) - Add the templates details to the `localized_templates_table` here.
    * [`app/assets/javascripts/projects/default_project_templates.js`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/app/assets/javascripts/projects/default_project_templates.js) - Add an entry for the template here for it to show in the UI. Look at other entries for guidence on how to add.
    * [`spec/lib/gitlab/sample_data_template_spec.rb`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/spec/lib/gitlab/sample_data_template_spec.rb) - Add the name of the template to the `all` method.
1. Export the project created in the previous steps as a tarball (`$name.tar.gz`) and add it to the [`vendor/sample_data_templates`](https://gitlab.com/gitlab-org/gitlab/-/tree/master/vendor/sample_data_templates) directory in your branch.
1. Run `bin/rake gettext:regenerate` in the `gitlab` project and commit new `.pot` file for translations
1. Add changelog (e.g. `bin/changelog -m 25486 "Add Sample Data template for .NET"`) as standard
1. When the project is ready for review, please create a new issue in [gitlab](https://gitlab.com/gitlab-org/gitlab/issues) with a link to your project.
    * In your issue, `@` mention the relevant Backend Engineering Manager and Product Manager for the [Templates category](https://about.gitlab.com/handbook/product/categories/#import-group).

#### 3a. Add a template list icon for the Sample Data project

As an additional optional step, to have the sample data project show with an icon on the templates list (not for the project itself when it's created, that's included in the tarball) do the following:

1. Add icon to the [gitlab-svgs](https://gitlab.com/gitlab-org/gitlab-svgs) project, as shown in [this example](https://gitlab.com/gitlab-org/gitlab-svgs/merge_requests/195).
1. Run `yarn run svgs` on `gitlab-svgs` project and commit result
1. Forward changes in `gitlab-svgs` project to master. This involves:
    * Merging your MR in `gitlab-svgs`
    * [renovate-gitlab-bot](https://gitlab.com/gitlab-org/frontend/renovate-gitlab-bot/) will pick the new release up and create an MR in `gitlab-org/gitlab`
1. Once the bot-created MR created above is merged, you can rebase your template MR onto the updated `master` to pick up the new svgs

## Contributing an improvement to an existing template

Existing templates are available in the [sample-data-templates](https://gitlab.com/gitlab-org/project-templates) group.

To contribute a change, please open a merge request in the relevant project and contact the Demo Team when you are ready for a review.
